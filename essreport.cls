% essreport.cls
% Report style for the European Spallation Source
% https://gitlab.esss.lu.se/latex/ess-latex
% Contact: Yngve.Levinsen@ess.eu

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{essreport}[2020/03/20 version 2.1 ESS report]

\LoadClass{extarticle} % load the base class

\RequirePackage{nomencl}
\renewcommand{\nomname}{Glossary}
% add the Glossary section and its number in ToC:
\def\thenomenclature{%
  \section{\nomname}
  \if@intoc\addcontentsline{toc}{section}{\nomname}\fi%
\nompreamble
\list{}{%
\labelwidth\nom@tempdim
\leftmargin\labelwidth
\advance\leftmargin\labelsep
\itemsep\nomitemsep
\let\makelabel\nomlabel}}

\RequirePackage{etoolbox}
\RequirePackage{multirow}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% uppercase sections in ToC
% https://tex.stackexchange.com/questions/113681/uppercase-sections-and-subsections-on-toc
\RequirePackage{regexpatch}
\xpatchcmd*{\@sect}{\fi#7}{\fi\@nameuse{format#1}{#7}}{}{}

%%% for sections and subsections we want uppercase
\protected\def\formatsection{\MakeUppercase}

%%% the other titles are left unchanged
\let\formatsubsection\@firstofone
\let\formatsubsubsection\@firstofone
\let\formatparagraph\@firstofone
\let\formatsubparagraph\@firstofone

%%% the following is necessary only if hyperref is used
\AtBeginDocument{%
  \pdfstringdefDisableCommands{%
    \let\formatsection\@firstofone
    \let\formatsubsection\@firstofone
  }%
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\newcounter{nowner}
\newcommand\setOwner[2]{% set owner and his/her affiliation
  \stepcounter{nowner}
  \csdef{owner\thenowner}{#1}
  \csdef{oaffil\thenowner}{#2}
}


\newcounter{ndistr}
\newcommand\setDistr[2]{% set distr and his/her affiliation
  \stepcounter{ndistr}
  \csdef{distr\thendistr}{#1}
  \csdef{aaffil\thendistr}{#2}
}

\newcounter{nreviewer}
\newcommand\setReviewer[2]{% set reviewer and his/her affiliation
  \stepcounter{nreviewer}
  \csdef{reviewer\thenreviewer}{#1}
  \csdef{raffil\thenreviewer}{#2}
}

\newcounter{napprover}
\newcommand\setApprover[2]{% set approver and his/her affiliation
  \stepcounter{napprover}
  \csdef{approver\thenapprover}{#1}
  \csdef{appaffil\thenapprover}{#2}
}

\newcommand\getName[2]{\csuse{#1#2}}
\newcommand\getAffil[2]{\csuse{#1#2}}

\newcounter{na}
\newcommand\listNames[3]{% #1-array size, #2 - name #3 - affiliation
  \setcounter{na}{0}%
  \whileboolexpr{ test {\ifnumcomp{\value{na}}{<}{#1-1}} }{\stepcounter{na} & \getName{#2}{\thena} & \getAffil{#3}{\thena} \\  }
  \stepcounter{na} &  \getName{#2}{\thena} & \getAffil{#3}{\thena}
}

\global\let\@type\@empty
\newcommand{\setType}[1]{\def\@type{#1}}
\let\type\setType %
\newcommand{\show@type}{\@type}


\global\let\@revision\@empty
\newcommand{\setRevision}[1]{\def\@revision{#1}}
\let\revision\setRevision %
\newcommand{\show@revision}{\@revision}

\global\let\@chess\@empty
\newcommand{\setChess}[1]{\def\@chess{#1}}
\let\chess\setChess %
\newcommand{\show@chess}{\@chess}

\global\let\@state\@empty
\newcommand{\setState}[1]{\def\@state{#1}}
\let\state\setState %
\newcommand{\show@state}{\@state}

\global\let\@confidentiality\@empty
\newcommand{\setConfidentiality}[1]{\def\@confidentiality{#1}}
\let\confidentiality\setConfidentiality %
\newcommand{\show@confidentiality}{\@confidentiality}


\def\@date{\today}
\newcommand{\setdate}[1]{\def\@date{#1}}
\newcommand{\show@date}{\@date}

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{extarticle}}

\ProcessOptions \relax



\newcommand{\head@table}{
  \begin{tabular}[t]{lp{6cm}}
    Document Type & \show@type  \\
    Document Number & \show@chess\\
    Date & \@date  \\
    Revision & \@revision \\ 
    State & \@state \\
    Confidentiality Level & \@confidentiality \\
    Page & \thepage~(\pageref{LastPage}) \\
  \end{tabular}
\end{tabular}
}

\renewcommand\maketitle{%
  \newgeometry{left=80pt, right=1cm, top=1cm, bottom=0cm}
  \begin{titlepage}
    {\scriptsize
      \begin{tabular}[t]{lr}
        \vtop{\vspace{-4pt}\null{\hbox{\includegraphics[width=0.21\textwidth]{figs/ESS_logotype_cyan_CMYK.eps}}}}
        &
          \hspace{4.5cm}
          \head@table
          }
          
          \vspace{4.cm}

          \hrule height 3pt width 0.924\linewidth \kern1pt \hrule height 0.7pt width 0.924\linewidth
          \par\kern20pt  
          \centering{\large\bf{\@title}}        
          \par\kern20pt
          \hrule height 0.7pt width 0.924\linewidth \kern1pt \hrule height 3pt width 0.924\linewidth 

          \vspace{1.59cm}
          \begin{flushleft}
            {\footnotesize
              \begin{tabular}{p{0.15\linewidth}p{0.25\linewidth}p{0.45\linewidth}}
                \toprule
                & {\bf Name} & {\bf Role/Title} \\
                \midrule
                \multirow{\thenowner}{*}{\bf \ifnumcomp{\value{nowner}}{>}{1}{Owners}{Owner} }
                \listNames{\thenowner}{owner}{oaffil}
                \\ \midrule
                \multirow{\thenreviewer}{*}{\bf \ifnumcomp{\value{nreviewer}}{>}{1}{Reviewers}{Reviewer} } 
                \listNames{\thenreviewer}{reviewer}{raffil}
                \\ \midrule
                \multirow{\thenapprover}{*}{\bf \ifnumcomp{\value{napprover}}{>}{1}{Approvers}{Approver}} 
                \listNames{\thenapprover}{approver}{appaffil}
                \ifnumcomp{\value{ndistr}}{>}{0}{
                \\ \midrule
                \multirow{\thendistr}{*}{\bf Distribution list} 
                \listNames{\thendistr}{distr}{aaffil}
                }{}
                \\ \bottomrule
              \end{tabular}
            }
          \end{flushleft}
        \end{titlepage}
        \restoregeometry 
      }

      \renewcommand*{\rmdefault}{cmss}

      % Page layout
      \RequirePackage{lastpage}
      \RequirePackage{booktabs}
      \RequirePackage{geometry}
      \newgeometry{left=80pt, right=65pt, top=2cm, bottom=2cm}

      \RequirePackage{graphicx}

      \RequirePackage{fancyhdr}
      \fancypagestyle{firstpage}{%
        \fancyhf{} % clear all six fields
        \renewcommand{\headrulewidth}{0pt}
        \renewcommand{\footrulewidth}{0pt}
      }
      \fancypagestyle{followingpage}{%
        \fancyhf{} % clear all six fields
        \fancyhead[L]{\scriptsize
          \begin{tabular}{lp{8cm}ll}
%            Report & \\
            Document Type & \show@type & Date & \@date \\
            Document Number & \show@chess & State & \@state \\
            Revision & \@revision & Confidentiality level & \@confidentiality \\
          \end{tabular}
        }
        \fancyfoot[C]{\thepage~(\pageref{LastPage})}
        \renewcommand{\headrulewidth}{0pt}
        \renewcommand{\footrulewidth}{0pt}
      }


      \pagestyle{followingpage}
      \AtBeginDocument{\thispagestyle{firstpage}}

