% This document class provides a simple memo for LaTeX users.
% It is based on article.cls and inherits most of the functionality
% that class.
%
% It was modified from the texMemo class by Rob Oakes, 2010.
% 
% This version was edited by P Bentley (phil.m.bentley@gmail.com) November 2014
% 
% Released under the LGPL, version 3.
% A copy of the LGPL can be found at http://www.gnu.org/licenses/lgpl.html

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{workorder}[2014/11/26 - Simple Memo Class, Including Logo]
\RequirePackage{palatino}

% Load the Base Class
\LoadClassWithOptions{article}

% Begin Requirements
\RequirePackage{ifthen}

% Specialized work order commands (To, From, Subject, Logo, effort estimate, completion date)

\def\@woto{\relax}
\newcommand{\woto}[1]{\gdef\@woto{#1}}

\def\@wofrom{\relax}
\newcommand{\wofrom}[1]{\gdef\@wofrom{#1}}

\def\@wosubject{\relax}
\newcommand{\wosubject}[1]{\gdef\@wosubject{#1}}

\def\@wodate{\relax}
\newcommand{\wodate}[1]{\gdef\@wodate{#1}}

\def\@wopcc{\relax}
\newcommand{\wopcc}[1]{\gdef\@wopcc{#1}}

\def\@woeffort{\relax}
\newcommand{\woeffort}[1]{\gdef\@woeffort{#1}}

\def\@wocompletedate{\relax}
\newcommand{\wocompletedate}[1]{\gdef\@wocompletedate{#1}}

\def\@wologo{\relax}
\newcommand{\wologo}[1]{\gdef\@wologo{\protect #1}}

\def\@letterheadaddress{\relax}
\newcommand{\lhaddress}[1]{\gdef\@letterheadaddress{#1}}

% Custom Document Formatting
\newcommand\decorativeline[1][1pt]{
	\par\noindent%
	\rule[0.5ex]{\linewidth}{#1}\par
}

% Set the Paper Size and margins
\RequirePackage{geometry}
\geometry{margin=1.0in}

% Create the Letterhead and To/From Block

\renewcommand{\maketitle}{\makewotitle}
\newcommand\makewotitle{
	\ifthenelse{\equal{\@wologo}{\relax}}{}
	{ % Create With Logo
	\begin{minipage}[t]{1\columnwidth}%
		\begin{flushleft}
			\vspace{-0.6in}
			\@wologo
			\vspace{0.5in}
		\par\end{flushleft}%
	\end{minipage}
	}
	
	\begin{center}
		\huge \@title
	\end{center}
	
	% To, From, Subject Block
	\begin{description}
		\ifthenelse{\equal{\@woto}{\relax}}{}{\item [{To:}] \@woto}
		\ifthenelse{\equal{\@wofrom}{\relax}}{}{\item [{From:}] \@wofrom}
		\ifthenelse{\equal{\@wosubject}{\relax}}{}{\item [{Subject:}] \@wosubject}
		\ifthenelse{\equal{\@wopcc}{\relax}}{}{\item [{PCC:}] \@wopcc}
		\ifthenelse{\equal{\@wodate}{\relax}}{}{\item [{Issue Date:}] \@wodate}
		\ifthenelse{\equal{\@woeffort}{\relax}}{}{\item [{Effort Estimate:}] \@woeffort}
		\ifthenelse{\equal{\@wocompletedate}{\relax}}{}{\item [{Expected Competion Date:}] \@wocompletedate}
	\end{description}
	
	\decorativeline\bigskip{}
}
