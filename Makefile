all: pdf png

pdf: examples/minutes.pdf examples/slides.pdf examples/report.pdf examples/requirements.pdf examples/workorder.pdf examples/poster.pdf
png: examples/minutes.png examples/slides.png examples/report.png examples/requirements.png examples/workorder.png examples/poster.png

LATEX_COMPILER = lualatex

examples/requirements.pdf: examples/requirements.tex essreqs.cls examples/requirements.csv examples/requirements-summary.txt
	@cp examples/requirements.csv examples/requirements-summary.txt .
	@$(LATEX_COMPILER) $<
	@rm requirements.csv requirements-summary.txt
	@mv requirements.pdf examples/

examples/minutes.pdf: examples/minutes.tex essminutes.cls
	@$(LATEX_COMPILER) $<
	@$(LATEX_COMPILER) $<
	@mv minutes.pdf examples/

examples/report.pdf: examples/report.tex essreport.cls
	@$(LATEX_COMPILER) $<
	@$(LATEX_COMPILER) $<
	@mv report.pdf examples/

examples/slides.pdf: examples/slides.tex  beamerinnerthemeESS.sty  beamerouterthemeESS.sty  beamerthemeESS.sty
	@$(LATEX_COMPILER) $<
	@mv slides.pdf examples/

examples/slides.png: examples/slides.pdf
	@convert -density 50 $<[0] $@

examples/poster.pdf: examples/poster.tex baposter.cls
	@$(LATEX_COMPILER) $<
	@$(LATEX_COMPILER) $<
	@mv poster.pdf examples/

examples/workorder.pdf: examples/workorder.tex workorder.cls
	@$(LATEX_COMPILER) $< && $(LATEX_COMPILER) $<
	@mv workorder.pdf examples/

%.png: %.pdf
	@convert -density 25 $<[0] $@

clean:
	rm -fv *.toc *.out *.log *.aux *.nav *.snm *.nls *.ilg *.nlo

distclean: clean
	rm -fv examples/*.pdf examples/*.png

attach: shielding-report.pdf
	pdftk shielding-report.pdf attach_files shielding-report.tex output shielding-report-with-attachment.pdf
