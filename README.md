# Some latex styles for the ESS documents

## Examples

| Slides   | Poster | Report | Minutes | Requirements | Work order |
|:--------:|:------:|:------:|:-------:|:------------:|:----------:|
| [![slides](/examples/slides.png)](/examples/slides.pdf) | [![poster](/examples/poster.png)](/examples/poster.pdf) | [![report](/examples/report.png)](/examples/report.pdf) | [![minutes](/examples/minutes.png)](/examples/minutes.pdf) | [![requirements](/examples/requirements.png)](/examples/requirements.pdf) | [![work order](/examples/workorder.png)](/examples/workorder.pdf) |


## Get it & compile it
```
wget https://gitlab.esss.lu.se/latex/ess-latex/-/archive/master/ess-latex-master.zip
unzip ess-latex-master.zip
cd ess-latex-master
make distclean
make
```

## Online Editors

We attempt to also update our online templates on Overleaf so you can directly copy those and start working. Please send feedback if you notice that some of them are out of date.

| Slides | Report | Article | Poster |
|:------:|:------:|:-------:|:------:|
| [Overleaf](https://www.overleaf.com/read/qbjmnjyfhhqw) | [Overleaf](https://www.overleaf.com/read/zfthkdkbcshs) | [Overleaf](https://www.overleaf.com/read/xfyshrcynnvn) | [Overleaf](https://www.overleaf.com/read/dnpbjcsvjxkt) |

## Contribute

Fork this project and send merge request, all contributions are welcome. No particular guidelines here for now. If you are unfamiliar with git you may also send corrections and updated files by e-mail or similar.

## Contact

Yngve Levinsen / ESS

This was formerly run by Konstantin Batkov who is not anymore working at ESS. Many thanks for his contributions and particular attention to details.

